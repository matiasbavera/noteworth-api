# README #

## Description of the problem
We need to use an external API and get a list of updated medical providers from this API and save them to a database. To make this connection and get the data, we must first authenticate with the /auth endpoint. After that, a call can be made to the endpoint /providers to get the list of providers.
    
The problem is that we cannot trust the API. The `/auth ` and `/providers` API endpoints might fail. The solution should retry a maximum of 2 times for each API before exiting successfully. Potential misbehaviors include API timeouts and incomplete response data.

For more information about the `/auth` and `/providers` API you can check this repo: https://github.com/datamindedsolutions/noteworth-challenge-api

## Solution
Creates an API that wraps the client's API and makes it more resilient

* Created a decorator that is in charge of executing a function returned in case the request fails.
* Created two managers, one `auth` and the other` providers` to handle the requests corresponding to each endpoint of the client API.
* Created one command to get providers.
* Created this the endpoint `/app/providers/`: Returns all the providers stored within the new API

Obs: The solution will retry two times for each endpoint.

## How do I get set up? ###

Setting up the virtual env

In your terminal: 

1. You can initialize a virtual python environment with your preferred tool (e.g: virtualenv).
2. Source your virtual env
3. `cd {your-project-folder}/scripts/`
4. `pip install -r requirements.txt`
5.  Go to the challenge's project folder running: `cd ../challenge/`
6. To create the tables on the database run `python3 manage.py migrate`.

## Run application
In your terminal:

1. Source your python virtual environment 
2. Go to your project folder and run: `cd challenge/`
3. For getting a token from the provider's API and for getting the list of providers you should run: `python manage.py get_providers` 

## Enter admin
Accessing the admin to check data:

1. Source your python virtual environment 
2. Go to your project folder and run: `cd challenge/`
3. Create a Django's superuser `python manage.py createsuperuser`
4. Run server with `python3 manage.py runserver localhost:8000`
5. In your browser access to http://localhost:8000/admin

## Configuration:
*  You can configure the API's url on the `sys_var.py` file.

# Tradeoffs
* Right now I'm just calling the providers API and checking if the provider doesn't exist in the database to be added. This does not contemplate the case that the information of an existing provider in the database changes. E.g .: one provider changes the clinic.
* Sqlite is being used because it makes setup easier. Depending on the scale of the project, it would be interesting to use postgres.


# TODOs

### Models
* Title can have its own model

### Functionality
* Add logs, is a good idea to verify which are the most frequent errors.

### Request Architecture 
* I could make a middleware to handle the authentication with the particular client. But I could also make a decorator that handles the authentication and specialize 
the decorator depending on the client. 

### UI
* The project could have have an UI with a button to get the providers. To do that we should add a url and a view.

### Add endpoints for this project
* Add an endpoint '/update_providers' to run the internal logic and get the providers from the external API.  
* We can use django-rest-framework to expose more endpoints.  

### Admin
* Improve views and add filters of models

### Tests
* Add unit tests.

### Deploy
* Add production and development settings file.

### Performance
* Although the early optimization is the root of all evil, If several users query for the same list of providers, it might be convenient to use cache for this list.
* Parallel tests if necessary
