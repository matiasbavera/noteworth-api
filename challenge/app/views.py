from django.http import HttpResponse
from django.core import serializers
# Create your views here.
from app.models import Provider


def get_providers(request):
    if request.method == 'GET':
        providers = Provider.objects.all()
        providers_json = serializers.serialize('json', providers)
        return HttpResponse(providers_json, content_type='application/json')