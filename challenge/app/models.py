from django.db import models


class Clinic(models.Model):
    name = models.CharField(max_length=30, unique=True)

    def save(self, *args, **kwargs):
        if self.name is None:
            return None
        self.name = self.name.lower()
        return super(Clinic, self).save(*args, **kwargs)


class Provider(models.Model):
    name_given = models.CharField(max_length=50)
    name_family = models.CharField(max_length=50)
    # Title can be its own model too.
    title = models.CharField(max_length=30, null=True)
    clinic = models.ForeignKey(Clinic, on_delete=models.PROTECT)

    class Meta:
        unique_together = ['name_given', 'name_family']

    def save(self, *args, **kwargs):
        self.name_given = self.name_given.lower()
        self.name_family = self.name_family.lower()
        return super(Provider, self).save(*args, **kwargs)


class APITokenService:
    def deactivate_tokens(self, client_name):
        for token in APIToken.objects.filter(client_name=client_name, active=True):
            token.active = False
            token.save()

    def get_active_token(self, client_name):
        active_token = APIToken.objects.filter(active=True, client_name=client_name)
        return active_token.last().token if active_token else None

class APIToken(models.Model):
    token = models.TextField()
    date = models.DateTimeField(auto_now_add=True)
    client_name = models.CharField(max_length=50, null=True)
    active = models.BooleanField(default=True)
    service = APITokenService()

    def save(self, *args, **kwargs):
        self.client_name = self.client_name.lower()
        return super(APIToken, self).save(*args, **kwargs)


