from django.core.management.base import BaseCommand
from app.views_helper.providers import Providers


class Command(BaseCommand):
    help = 'Gets the provider list'

    def handle(self, *args, **options):
        print('getting the provider`s list')
        provider_handler = Providers()
        provider_handler.update_provider_list()

