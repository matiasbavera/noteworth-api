from hashlib import sha256
import requests


# This class will create the standard retry decorator.
# It only retries on a 500 status code.
class Retry:
    # By default, retry up to this many times.
    MAX_TRIES = 2

    # This method holds the validation check.
    def is_error(self, resp):
        # This will retry on *any* 5xx error
        return resp.status_code >= 500

    def __call__(self, func):
        def retried_func(*args, **kwargs):
            tries = 0
            resp = {}
            while True:
                if tries > self.MAX_TRIES:
                    print('Run the script again. Reached the max amount of retries')
                    break

                try:
                    resp = func(*args, **kwargs)
                except requests.exceptions.ReadTimeout:
                    print('Error:Caught a timeout')
                    tries += 1
                    continue

                if not self.is_error(resp):
                    break

                tries += 1
                print('An error has ocurred. Retrying')
            return resp

        return retried_func


class RetryOnInternalServerAndJsonError(Retry):
    def is_error(self, resp):
        try:
            resp.json()
        except Exception as e:
            print('Run the script again. It`s not json serializable ' + str(e))
            return True

        if resp.status_code >= 500:
            return True

        return False


# Decorator functions.
retry_on_server_error = Retry()
retry_on_internal_server_and_json_error = RetryOnInternalServerAndJsonError()


def get_checksum_hex(token, url):
    return sha256((str(token) + url).encode('utf-8')).hexdigest()