import requests
from django.db import IntegrityError

from app.models import Clinic, Provider, APIToken
from app.sys_var import PROVIDERS_API_URL
from app.utils.request_utils import get_checksum_hex, retry_on_internal_server_and_json_error
from app.views_helper.auth import RetrieveAuthToken


class Providers:

    @retry_on_internal_server_and_json_error
    def _get_list_of_providers(self, token):
        url = PROVIDERS_API_URL + 'providers'
        # Getting last active stored token
        token_checksum_hex = get_checksum_hex(token, '/providers')
        headers = {'X-Request-Checksum': token_checksum_hex}
        return requests.get(url, headers=headers, timeout=10)


    def update_provider_list(self):
        token = APIToken.service.get_active_token('provider')
        if not token:
            """
            Calling /auth will invalidate any old tokens and generate a new token. 
            """
            print('There is no active token, getting the token')
            token_handler = RetrieveAuthToken()
            token = token_handler.store_token()

        response = self._get_list_of_providers(token)

        if response.status_code >= 400 and response.status_code < 500:
            print('You should authenticate before try to to get the list of providers')
            return

        provider_object = response.json()
        provider_list = provider_object['providers']
        #with transaction.atomic():
        for provider in provider_list:
            clinic, created = Clinic.objects.get_or_create(name=provider['clinic'].lower())
            data = Provider()
            data.clinic = clinic
            data.name_given = provider['name_given']
            data.name_family = provider['name_family']
            data.title = provider['title']
            try:
                data.save()
            except IntegrityError:
                print('This provider is already registered')