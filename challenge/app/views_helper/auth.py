from django.db import transaction
import requests

from app.models import APIToken
from app.sys_var import PROVIDERS_API_URL
from app.utils.request_utils import retry_on_server_error


class RetrieveAuthToken:
    @retry_on_server_error
    def _get_token(self, url):
        return requests.get(url + 'auth', timeout=10)

    def store_token(self):
        api_token = ''
        # Deactivate all the tokens of a specific API
        APIToken.service.deactivate_tokens('provider')
        try:
            api_token = self._get_token(PROVIDERS_API_URL).headers['Super-Secure-Token']
        except Exception as e:
            print('Run the script again: An Error has occurred getting the token ' + str(e))
            return

        if not api_token:
            print('Could not get token')
            return

        with transaction.atomic():
            token = APIToken()
            token.token = api_token
            token.client_name = 'provider'
            token.save()

        return token
        print('Got the token')