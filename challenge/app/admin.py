from django.contrib import admin

# Register your models here.
from app.models import Clinic, Provider, APIToken

admin.site.register(Clinic)
admin.site.register(Provider)
admin.site.register(APIToken)