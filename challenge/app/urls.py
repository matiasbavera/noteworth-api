from django.urls import path

from app.views import get_providers

urlpatterns = [
    path('providers/', get_providers, name='providers'),
]